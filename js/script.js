
const toggleButton = document.getElementById('theme-toggle');
const body = document.body;
const themeKey = 'selected-theme'; // ключ для зберігання теми в локальному сховищі

// Відновлюємо збережену тему при завантаженні сторінки
const savedTheme = localStorage.getItem(themeKey);
if (savedTheme) {
    body.classList.add(savedTheme);
    if (savedTheme === 'dark') {
        toggleButton.style.color = '#4bcaff';
    }
}

toggleButton.addEventListener('click', () => {
    body.classList.toggle('dark');
    body.classList.toggle('light');

    // Зберігаємо вибрану тему в локальному сховищі
    const selectedTheme = body.classList.contains('dark') ? 'dark' : 'light';
    localStorage.setItem(themeKey, selectedTheme);

    // Змінюємо колір тексту кнопки залежно від теми
    if (selectedTheme === 'dark') {
        toggleButton.style.color = '#35444f';
    } else {
        toggleButton.style.color = '#4bcaff';
    }
});

